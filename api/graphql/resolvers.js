import { connect } from "tls";

const fetch = require("node-fetch");

const FortuneCookie = {
    getOne() {
        return fetch("http://fortunecookieapi.herokuapp.com/v1/cookie")
            .then(res => res.json())
            .then(res => {
                return res[0].fortune.message;
            });
    }
};

function comparaData(date1, date2) {
    //dia/mes/ano
    //hora:minuto
    let d1 = parseInt(`${date1.split('/')[2]}` + `${date1.split('/')[1]}` + `${date1.split('/')[0]}`)
    let d2 = parseInt(`${date2.split('/')[2]}` + `${date2.split('/')[1]}` + `${date2.split('/')[0]}`)
    // let firstDate = Date.parse(date1)
    // let secondDate = Date.parse(date2)

    //   if (firstDate > secondDate)
    //     return -1
    //   if (firstDate < secondDate)
    //     return 1
    //   return 0
    // }
    if (d1 < d2) return 1;
    if (d1 > d2) return -1;
    return 0;
}

const resolvers = {
    Query: {
        getEditions: (_, args, context, info) => {
            return context.prisma.query.editions(
                {
                    orderby: "date_start_DESC",
                    first: args.quantity
                },
                info
            )
        },
        getHackathons: (_, args, context, info) => {
            return context.prisma.query.hackathons({ orderBy: "id_DESC" }, info);
        }
    },
    Mutation: {
        newHackathon: (_, args, context, info) => {
            return context.prisma.mutation.createHackathon(
                {
                    data: {
                        name: args.name
                    }
                },
                info
            );
        },
        newEdition: async (_, args, context, info) => {
            const errors = {
              hackDate: `A "data de início" do Hackathon deve ser anterior à "data de término" do Hackathon`,
              regDate:  `A "data de início" da inscrição deve ser anterior à "data de término" da inscrição`,
              regHackDate: `A "data de início" do Hackathon deve ser posterior à "data de término" da inscrição`,
              regTodayDate: `A "data de início" do Hackathon deve ser posterior à "data de hoje"`,

              nullHackDateStart: `A "data de início" do Hackathon não pode ser vazia`,
              nullHackDateEnd: `A "data de término" do Hackathon não pode ser vazia`,
              nullRegDateStart: `A "data de início" da inscrição não pode ser vazia`,
              nullRegkDateEnd: `A "data de término" da inscrição não pode ser vazia`,
              nullId: 'ID do hackathon não existe',

            }
            if (comparaData(args.date_start, args.date_end) == -1)
              throw new Error(errors.hackDate)
            if (comparaData(args.reg_period_start, args.reg_period_end) == -1)
              throw new Error(errors.regDate)
            if (comparaData(args.reg_period_end, args.date_start) == -1)
              throw new Error(errors.regHackDate)

            if (args.date_start == null)
              throw new Error(nullHackDateStart)
            if (args.date_end == null)
              throw new Error(nullHackDateEnd)
            if (args.reg_period_start == null)
              throw new Error(errors.nullregDateStart)
            if (args.reg_period_end == null)
              throw new Error(errors.nullregkDateEnd)
            if (args.hackathonId == null)
              throw new Error(errors.nullId)


            const hacks = await context.prisma.query.hackathons(
            {
              where: {
                id: args.hackathonId
              }
            })
            if (!hacks.length){
              throw new Error("Não existe um Hackathon com esse ID!")
            }

            return context.prisma.mutation.createEdition(
                {
                    data: {
                        description: args.description,
                        location: args.location,
                        requirement: args.requirement,
                        reg_period_start: args.reg_period_start,
                        reg_period_end: args.reg_period_end,
                        date_start: args.date_start,
                        date_end: args.date_end,
                        theme: args.theme,
                        rules: args.rules,
                        hackathon: {
                            connect: {
                                id: args.hackathonId
                            }
                        }
                    }
                },
                info
            );
        }
    }
};

export default resolvers;
